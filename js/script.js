

// JavaScript code for game logic
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

let wincon = false;


  
function clickDpadYellow() {
    keys['ArrowUp'] = true;
    player.y -= playerSpeed*2;
}

function clickDpadBlue() {
    keys['ArrowLeft'] = true;
    player.x -= playerSpeed;
}

function clickDpadRed() {
    keys['ArrowRight'] = true;
    player.x += playerSpeed;
}

function clickDpadGreen() {
    keys['ArrowDown'] = true;
    player.y += playerSpeed
}

function clickableDpadReleased() {
    keys['ArrowUp'] = false;
    keys['ArrowLeft'] = false;
    keys['ArrowRight'] = false;
    keys['ArrowDown'] = false;
}
let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];

const backgrounds= {
    background1src: 'images/background1.jpg',
    background2src: 'images/background2.jpg',
    background3src: 'images/background3.jpg',
    background4src: 'images/background4.jpg',
    background5src: 'images/background5.jpg',
    // background6src: 'images/background6.jpg',
    // background7src: 'images/background7.jpg',
    // background8src: 'images/background8.jpg',
    // background9src: 'images/background9.jpg',
    // background10src: 'images/background10.jpg',
    // background11src: 'images/background11.jpg',
    background12src: 'images/background12.jpg',
    
}

const SpriteLogic = {
    Sprite1: {x:0, y:0, width:887, height:621},
    Sprite2: {x:1026, y:0, width:1096, height: 672},
    Sprite3:{x:2302, y:0, width: 969, height: 635},
    Sprite4:{x:0, y:621, width: 1206, height: 596},
    Sprite5:{x:1206, y:672, width: 674, height: 674},
    Sprite6:{x:2302, y:635, width: 806, height: 709},
    Sprite7:{x:0, y:1217, width: 1137, height: 520},
    Sprite8:{x:1206, y:1346, width: 1068, height: 773},
    Sprite9:{x:2302, y:1344, width: 964, height:707},
    Sprite10:{x:0, y:1737, width: 1074, height: 769},
    Sprite11:{x:1206, y:2119, width: 926, height: 806},
    Sprite12:{x:2302, y:2051, width: 1056, height: 401}
}

class GameObject {
    constructor(spritesheet, x, y, width, height) {
        this.spritesheet = spritesheet;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    draw(ctx, currentSprite) {
        const sprite = SpriteLogic[`Sprite${currentSprite}`];
        

        if (sprite) {
            ctx.fillRect(this.x, this.y, this.width, this.height);
            ctx.drawImage(
                this.spritesheet,
                sprite.x,
                sprite.y,
                sprite.width,
                sprite.height,
                this.x,
                this.y,
                this.width,
                this.height
            );
        }
    }
}
const allPlatforms = [
    // Platforms for level 1
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
        { x: 100, y: canvas.height - 550, width: 100, height: 20 },
        { x: 400, y: canvas.height - 450, width: 150, height: 20 },
        { x: 300, y: canvas.height - (450/2), width: 150, height: 20 }
       
        // Add more platforms for level 1
    ],
    // Platforms for level 2
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 50 },
        { x: 150, y: canvas.height - 520, width: 120, height: 20 }
    ],
    // Platforms for level 3
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
        { x: 400, y: canvas.height - 350, width: 100, height: 20 },
        { x: 250, y: canvas.height - 500, width: 130, height: 20 }
    ],
    // Platforms for level 4
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
        { x: 200, y: canvas.height - 600, width: 80, height: 20 },
        { x: 320, y: canvas.height - 480, width: 110, height: 20 }
    ],
    // Platforms for level 5
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ],
    // Platforms for level 6
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ],
    // Platforms for level 7
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ],
        // Platforms for level 8
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ],
        // Platforms for level 9
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ],
        // Platforms for level 10
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ],
        // Platforms for level 11
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ],
        // Platforms for level 12
    [
        { x: 100, y: canvas.height - 50, width: 100, height: 20 },
        { x: 300, y: canvas.height - 100, width: 150, height: 20 },
        { x: 500, y: canvas.height - 150, width: 120, height: 20 },
        { x: 700, y: canvas.height - 200, width: 100, height: 20 },
    ]
];

let currentLevel = 1; // Current level index


////////////////////////////////////
//image loading
////////////////////////////////////
let spriteSpriteSheet = new Image()
spriteSpriteSheet.src = 'images/SpriteLogohistory.png';//spritesheet
let player = new GameObject(spriteSpriteSheet, 56, canvas.height - 100, 100, 100);

let background = new Image()
background.src = 'images/background1.jpg'
let playerSpeed = 5; // used to adjust movement speed

// Object to track pressed keys
const keys = {};

const gravity = 0.5; // Adjust gravity strength as needed
let verticalSpeed = 0; // Vertical speed
const terminalVelocity = 10; // Maximum falling speedlet jumpStrength = -10; // Adjust jump strength as needed




window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);


const backgroundAudio = document.getElementById('backgroundAudio');

// Play audio
function playBackgroundMusic() {
  backgroundAudio.play();
}

// Pause audio
function pauseBackgroundMusic() {
  backgroundAudio.pause();
}


// Update player position based on pressed keys
function updatePlayerPosition() {
// Apply gravity
    if (player.y < canvas.height - player.height) {
        verticalSpeed += gravity;
        if (verticalSpeed > terminalVelocity) {
            verticalSpeed = terminalVelocity;
        }
        player.y += verticalSpeed;
    } else {
        // Reset vertical speed when player is on the ground
        verticalSpeed = 0;
        player.y = canvas.height - player.height; // Ensure player sticks to the ground
    }

    // Handle collisions with platforms
     // Check if allPlatforms[currentLevel] exists before using forEach
         if (Array.isArray(allPlatforms[currentLevel])) {
             const levelPlatforms = allPlatforms[currentLevel];
             levelPlatforms.forEach((platform) => {
                 // Calculate player's next position
                 let nextX = player.x;
                 let nextY = player.y;
                 
                 if (
                     nextX < platform.x + platform.width &&
                     nextX + player.width > platform.x &&
                     nextY < platform.y + platform.height &&
                     nextY + player.height > platform.y
                     ) {
                         // Resolve collision by adjusting player's position and speed
                         if (verticalSpeed > 0) { // Check if moving downwards
                            verticalSpeed = 0;
                            player.y = platform.y - player.height;
                        } else if (verticalSpeed < 0) { // Check if moving upwards
                            verticalSpeed = 0;
                            player.y = platform.y + platform.height;
                        }
                    }
                });
            ;
        } else {
            console.log(`Level ${currentLevel} platforms are undefined.`);
        }
                
    if (keys.ArrowLeft) {
        player.x -= playerSpeed;
    }
    if (keys.ArrowRight) {
        player.x += playerSpeed;
    }
    if (keys.ArrowUp) {
        player.y -= playerSpeed*2;
    }
    if (keys.ArrowDown) {
        player.y += playerSpeed;
    }
}


let spriteX = 0; // X-coordinate of sprite
let spriteY = 0; // Y-coordinate of sprite
const spriteWidth = 498; // Width of a single  of the portal
const spriteHeight = 498; // Height of a single frame of the portal
////////////////////////////////////
let spriteXForSpriteSheet = 0;
let spriteyForSpriteSheet = 0;
//////////////////////////////
let frame = 0; // Current frame
const totalFrames = 9; // Total frames in the spritesheet
//////////////////////////////
const spriteImageBluePortal = new Image();//make blue portal image
const spriteImageOrangePortal = new Image();//make orange portal image
spriteImageBluePortal.src = 'images/portalBlue.png';
spriteImageOrangePortal.src= 'images/portalOrange.png';

// Control FPS of the animation
let fps = 10; // Adjust FPS as needed
let frameDelay = 1000 / fps; // Delay between frame updates in milliseconds
let lastFrameTime = 0;

// Update sprite animation frame
function updateFrame(currentTime) {
    if (currentTime - lastFrameTime >= frameDelay) {
        frame = (frame + 1) % totalFrames;
        spriteX = frame * spriteWidth; // Update X-coordinate for the next frame
        lastFrameTime = currentTime;
    }
}

function checkCollision(rect1, rect2) {
    return rect1.x < rect2.x + rect2.width &&
           rect1.x + rect1.width > rect2.x &&
           rect1.y < rect2.y + rect2.height &&
           rect1.y + rect1.height > rect2.y;
}

changeFPS(30)//animation at 30 fps

function update(currentTime){
    // console.log("updating");
    if (currentLevel == 12) {
        wincon = true;
    }
     //Check collision between player and blue portal
    if (player.x < 50){
        if(player.y > canvas.height - 150) {
            console.log("collision detected on blue")
            // Teleport the player to the other portal's center
            player.x = canvas.width - 100; // Adjust according to the orange portal's position
            player.y = 174  - player.height / 2;// Adjust according to the orange portal's position
            if (currentLevel > 1) {
                currentLevel--;
                console.log(currentLevel);
            }
        }
    }
    if (player.x > canvas.width - 100){
        if(player.y < 249) {
            console.log("collision detected on orange")
            // Teleport the player to the other portal's center
            player.x = 50; // According to the orange portal's position
            player.y = canvas.height - 174 + 100 - player.height / 2; // Adjust according to the orange portal's position
            if (currentLevel < 12) {
                currentLevel++;
                console.log(currentLevel);
            }
        }  
    }
    switch (currentLevel) {
        case 1:
            background.src = 'images/background1.jpg';
            break;
        case 2:
            background.src = 'images/background1.jpg';
            break;
        case 3:
            background.src = 'images/background2.jpg';
            break;
        case 4:
            background.src = 'images/background2.jpg';
            break;
        case 5:
            background.src = 'images/background3.jpg';
            break;
        case 6:
            background.src = 'images/background3.jpg';
            break;
        case 7:
            background.src = 'images/background4.jpg';
            break;
        case 8:
            background.src = 'images/background4.jpg';
            break;
        case 9:
            background.src = 'images/background5.jpg';
            break;
        case 10:
            background.src = 'images/background5.jpg';
            break;
        case 11:
            background.src = 'images/background5.jpg';
            break;
        case 12:
            background.src = 'images/background5.jpg';
            break;
        
        default:
            break;
    }
    // Update sprite frame for animation
    updateFrame(currentTime);
    // Update player position
    updatePlayerPosition();

}

// Function to draw platforms for the current level
function drawPlatformsForCurrentLevel() {
        if (Array.isArray(allPlatforms[currentLevel])) {
            const levelPlatforms = allPlatforms[currentLevel];
            levelPlatforms.forEach((platform) => {
                ctx.fillStyle = 'green'; // Set platform color
                ctx.fillRect(platform.x, platform.y, platform.width, platform.height);
                
            });
        }
        else {
            console.log(`Platforms for Level ${currentLevel} are undefined.`);
        }
    }


function draw() {
    switch (wincon) {
        case true:
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = 'Green'
            ctx.fillRect(0, 0, canvas.width, canvas.height)
            ctx.fillStyle = 'white';
            ctx.font = "50px Future";
            ctx.fillText("You Win", canvas.width/2-100, canvas.height/2 - 50);
            break;
        case false:
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(background, 0, 0, canvas.width, canvas.height)
            ctx.font = "70px Old";
            ctx.fillStyle = 'white';
            ctx.fillText("Past", 10, 300);
            ctx.font = "50px Future";
            ctx.fillText("Future", canvas.width - 170, 250);
            
            // Draw the first sprite (orange portal)
            ctx.drawImage(spriteImageOrangePortal, spriteX, spriteY, spriteWidth, spriteHeight, canvas.width - 140, 0, 200, 200); // Adjust coordinates as needed
            // Move the origin to the center of the sprite and apply an offset for positioning
            ctx.drawImage(spriteImageBluePortal, spriteX, spriteY, spriteWidth, spriteHeight, -30, canvas.height-174, 200, 200); // Adjust coordinates as needed
            
            ctx.fillStyle = 'green'; // Set platform color
            drawPlatformsForCurrentLevel();
        
            // Restore the context state to revert the transformations
            player.draw(ctx, currentLevel);
        break;
    }
}

// Function to change the FPS
function changeFPS(newFPS) {
    fps = newFPS;
    frameDelay = 1000 / fps; // Recalculate frame delay based on new FPS
}

function gameloop(currentTime) {
    update(currentTime);
    draw();
    requestAnimationFrame(gameloop); // Pass the gameloop function for recursion
}
// Start the game loop
spriteImageBluePortal.onload = function() {
    spriteImageOrangePortal.onload = function() {
        // Start the game loop once the images are loaded
        gameloop();
    };
};
// Event listeners to track key presses
// document.addEventListener('keydown', (event) => {
//     keys[event.key] = true;
// });

// document.addEventListener('keyup', (event) => {
//     keys[event.key] = false;
// });


// Keep the event listeners for button clicks
yellowButton.addEventListener('mousedown', clickDpadYellow);
yellowButton.addEventListener('mouseup', clickableDpadReleased);

blueButton.addEventListener('mousedown', clickDpadBlue);
blueButton.addEventListener('mouseup', clickableDpadReleased);

redButton.addEventListener('mousedown', clickDpadRed);
redButton.addEventListener('mouseup', clickableDpadReleased);

greenButton.addEventListener('mousedown', clickDpadGreen);
greenButton.addEventListener('mouseup', clickableDpadReleased);
/*
for (let i = 0; i < obstacleCount; i++) {
    const obstacleWidth = Math.floor(Math.random() * 50) + 30; // Random width between 30 and 80
    const obstacleHeight = Math.floor(Math.random() * 50) + 30; // Random height between 30 and 80
    const obstacleX = Math.floor(Math.random() * (canvas.width - obstacleWidth)); // Random X-coordinate within canvas width
    const obstacleY = Math.floor(Math.random() * (canvas.height - obstacleHeight)); // Random Y-coordinate within canvas height

    obstacles.push({ x: obstacleX, y: obstacleY, width: obstacleWidth, height: obstacleHeight });
  }


*/